package dawid.siwko.constitution;

import java.util.List;

public interface IConstitution {
    Preamble getPreamble();
    Chapter getChapter(int number);
    Article getArticle(int number);
}
