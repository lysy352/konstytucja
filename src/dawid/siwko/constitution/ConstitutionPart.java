package dawid.siwko.constitution;

/**
 * Created by Dawid Siwko on 01.12.2016.
 */
public enum ConstitutionPart {
    Preamble, Chapter, Title, Article
}
